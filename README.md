
# Docker course (2 days)

## Main Goal

The basics of Docker has a steep learning path. This course is ment to guide you through this first part, learn the basics and learn it right. The goal is to enable you to make the next steps yourself.

## Labs

You learn Docker by doing. Each day consist of multiple labs where you learn hands on.

## Prerequisites

The preffered setup is that you have docker (Docker CE or Docker Desktop) installed on your own machine, because:

- you can apply what you have learned in your work more easily.
- you can use your favourite editor to edit files.

Alternative: If you can't install Docker you can use the provided VM's to work on, which you can reach via a ssh client, e.g. Putty, Mobaxterm, or ssh command line tool. 

## Infra

The following will be provided:

- Ubuntu VM's containing all commandline tools you need
- Container registry

## Agenda

### Day 1

- What are containers
  - proces with boundaries
- Why containers
- How to use
  - Build
    - Dockerfile
    - Docker CLI
    - Build images
  - Run (locally)
  
### Day 2

- Ship
  - Registries
  - Push / pull
- Run
  - volumes
  - network
- Orchestration
  - compose
