# Lab - Web app Nginx

ToDo

- Inspect Dockerfile
- build image
  - `docker build -t webapp .`
  - Compare Dockerfile to build output
- verify if image works
  - run container
    - in background
    - publish port on host port 8080
  - browse to localhost:8080

## Learning goals

- Get familiar with 
  - Long running container
  - publishing ports
  - locally running web apps
- Get used to
  - build images
  - run a container locally
